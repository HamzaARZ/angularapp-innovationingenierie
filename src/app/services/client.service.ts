import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { IClient } from 'src/app/models/client';
import { Observable } from 'rxjs';//import { Observable, throwError } from 'rxjs';
import { throwError} from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  private _clientUrl = environment.apiUrl + "/clients";
  public headers: HttpHeaders;

  constructor(private http : HttpClient,
    private authService: AuthService) { }

  getClients(): Observable<IClient[]> {
    this.headers = this.authService.getTokenHeader();

    return this.http.get<IClient[]>(this._clientUrl, {headers: this.headers}).pipe(catchError(this.errorHandler));
  }
  
  getClientById(clientId: String): Promise<IClient> {
    this.headers = this.authService.getTokenHeader();
    return this.http.get<IClient>(this._clientUrl + "/" + clientId, {headers: this.headers}).toPromise();
  }
  
  errorHandler(error : HttpErrorResponse){
    return throwError(error || "server error !!");
  }

  createOrUpdateClient(client: IClient): Observable<any> {
    this.headers = this.authService.getTokenHeader();
    return this.http.post(this._clientUrl, client, {headers: this.headers}).pipe(catchError(this.errorHandler));
  }


  deleteClient(clientId: String): Observable<any> {
    this.headers = this.authService.getTokenHeader();
    return this.http.delete<any>(this._clientUrl + "/" + clientId, {headers: this.headers}).pipe(catchError(this.errorHandler));
  }
 
}