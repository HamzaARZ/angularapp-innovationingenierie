import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { IProject } from 'src/app/models/project';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private _projectUrl = environment.apiUrl + "/projects";
  private _fileUrl = environment.apiUrl + "/files";
  private downloadFileUrl = environment.apiUrl + "/download_file";

  public headers: HttpHeaders;

  constructor(private http: HttpClient,
    private authService: AuthService) { }


  getProjects(): Observable<IProject[]> {
    this.headers = this.authService.getTokenHeader();
    return this.http.get<IProject[]>(this._projectUrl, { headers: this.headers }).pipe(catchError(this.errorHandler));
  }

  getProjectById(projectId: String): Promise<IProject> {
    this.headers = this.authService.getTokenHeader();
    return this.http.get<IProject>(this._projectUrl + "/" + projectId, {headers: this.headers}).toPromise();
  }

  createOrUpdateProject(project: IProject): Observable<any> {
    this.headers = this.authService.getTokenHeader();
    // ".pipe(catchError(this.errorHandler))" added
    return this.http.post(this._projectUrl, project, { headers: this.headers }).pipe(catchError(this.errorHandler));
  }

  deleteProject(projectId: String): Observable<any> {
    this.headers = this.authService.getTokenHeader();
    return this.http.delete(this._projectUrl + "/" + projectId, { headers: this.headers }).pipe(catchError(this.errorHandler));
  }

  // downloadFile(projectId: String, file: String) {
  //   this.headers = this.authService.getTokenHeader();
  //   this.headers.append('responseType', 'blob');
  //   let downloadFileUrl = this.downloadFileUrl + "/" + projectId + "/" + file;
  //   return this.http.get(downloadFileUrl, {headers: this.headers});
  // }

  downloadFile(projectId: String, file: string) {
    let anchor = document.createElement("a");
    document.body.appendChild(anchor);
    let downloadFileUrl = this.downloadFileUrl + "/" + projectId + "/" + file;

    let token = "Bearer " + this.authService.getToken();
    
    let headers = new Headers();
    headers.append('Authorization', token);

    fetch(downloadFileUrl, { headers })
      .then(response => response.blob())
      .then(blobby => {
        let objectUrl = window.URL.createObjectURL(blobby);

        anchor.href = objectUrl;
        anchor.download = file;
        anchor.click();

        window.URL.revokeObjectURL(objectUrl);
      });
  }




  deleteFile(projectId: String, file: String): Observable<any> {
    this.headers = this.authService.getTokenHeader();
    let deleteFileUrl = this._fileUrl + "/" + projectId + "/" + file;
    return this.http.delete(deleteFileUrl, { headers: this.headers }).pipe(catchError(this.errorHandler));
  }


  errorHandler(error: HttpErrorResponse) {
    return throwError(error || "server error !!");
  }

  // pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
  //   const formData = new FormData();
  //   formData.append('file', file);
  //   const newRequest = new HttpRequest('POST', 'http://localhost:8080/upload_files/d9dd6a67-e475-41d3-972b-e82477d6e04e', formData, {
  //   reportProgress: false,
  //   responseType: 'text'
  //   });
  //   return this.http.request(newRequest);
  //   }
  pushFileToStorage(files: File[], projectId: String): Observable<any> {
    this.headers = this.authService.getTokenHeader();
    const url = 'http://localhost:8080/upload_files/' + projectId;
    const formData = new FormData();
    for (let file of files) {
      formData.append('file', file);
    }

    //const uploadFilesRequest = new HttpRequest('POST', , formData, {responseType: 'text'});
    return this.http.post(url, formData, { headers: this.headers });
  }

  getProjectFiles(projectId: String): Observable<String[]> {
    this.headers = this.authService.getTokenHeader();
    let projectFilesUrl = this._fileUrl + "/" + projectId;
    return this.http.get<String[]>(projectFilesUrl, { headers: this.headers }).pipe(catchError(this.errorHandler));
  }
}
