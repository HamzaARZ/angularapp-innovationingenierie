import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { throwError} from 'rxjs';
import { catchError } from 'rxjs/operators';

import { LoginRequest } from '../models/loginRequest'
import { RegisterRequest } from '../models/registerRequest';
import { Router } from '@angular/router';

export const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    // 'Access-Control-Allow-Credentials' : 'false',
    'Access-Control-Allow-Origin': '*',
    "Access-Control-Allow-Methods": "DELETE, POST, GET, OPTIONS",
    "Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public token;

  constructor(private http : HttpClient,
    private router: Router) { }

  login(loginRequest: LoginRequest): Observable<any> {
    return this.http.post('http://localhost:8080/login', loginRequest);
  }

  register(registerRequest: RegisterRequest): Observable<any>{
    return this.http.post('http://localhost:8080/users', registerRequest)
      .pipe(catchError(this.errorHandler));
  }

  reconnect(){
    this.logout();
    this.router.navigateByUrl("/login");
  }

  errorHandler(error : HttpErrorResponse){
    return throwError(error);
  }

  getTokenHeader(): HttpHeaders{
    this.token = "Bearer " + this.getToken();
    let authHeader = {'Authorization': this.token};
    let headers: HttpHeaders = new HttpHeaders(authHeader);
    headers.append('observe', 'response');
    return headers;
  }


  hasAuthority(Authority: string): Boolean{
    if(this.getAuthorities() === null){
      return false;
    }
    return this.getAuthorities().includes(Authority);
  }

  isLoggedIn(): Boolean{
    return localStorage.getItem('token') !== null;
  }

  logout(){
    this.removeToken();
    this.removeUsername();
    this.removeAuthorities();
  }


  setToken(token){
    localStorage.setItem('token', token);
  }
  getToken(){
    return localStorage.getItem('token');
  }
  removeToken(){
    localStorage.removeItem('token');
  }

  setUsername(username){
    localStorage.setItem('username', username);
  }
  getUsername(){
    return localStorage.getItem('username');
  }
  removeUsername(){
    localStorage.removeItem('username');
  }
  
  setAuthorities(Authorities){
    localStorage.setItem('Authorities', Authorities);
  }
  getAuthorities(){
    return localStorage.getItem('Authorities');
  }
  removeAuthorities(){
    localStorage.removeItem('Authorities');
  }
}