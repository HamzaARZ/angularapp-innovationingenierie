import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { RegisterRequest } from '../models/registerRequest';
import { IUser } from '../models/user';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private usersUrl = environment.apiUrl + "/users";
  public headers: HttpHeaders;

  constructor(private http : HttpClient,
    private authService: AuthService) { }

  getUsers(): Observable<IUser[]> {
    this.headers = this.authService.getTokenHeader();
    return this.http.get<IUser[]>(this.usersUrl, {headers: this.headers}).pipe(catchError(this.errorHandler));
  }

  getUserByUsername(username: string): Promise<IUser>{
    this.headers = this.authService.getTokenHeader();
    return this.http.get<IUser>(this.usersUrl + "/" + username, {headers: this.headers}).toPromise();
  }


  activateUser(username: string): Observable<any>{
    this.headers = this.authService.getTokenHeader();
    return this.http.put<any>(this.usersUrl + "/activate/" + username, null, {headers: this.headers}).pipe(catchError(this.errorHandler));
  }
  disableUser(username: string): Observable<any>{
    this.headers = this.authService.getTokenHeader();
    return this.http.put<any>(this.usersUrl + "/disable/" + username, null, {headers: this.headers}).pipe(catchError(this.errorHandler));
  }
  

  updateUser(username: string, registerRequest: RegisterRequest): Observable<any>{
    this.headers = this.authService.getTokenHeader();
    return this.http.put<any>(this.usersUrl + "/" + username, registerRequest, {headers: this.headers}).pipe(catchError(this.errorHandler));
  }

  deleteUser(username: String): Observable<any> {
    this.headers = this.authService.getTokenHeader();
    return this.http.delete<any>(this.usersUrl + "/" + username, {headers: this.headers}).pipe(catchError(this.errorHandler));
  }

  errorHandler(error : HttpErrorResponse){
    return throwError(error || "server error !!");
  }
}
