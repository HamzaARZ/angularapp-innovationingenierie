import { UserRole } from './UserRole'

export interface RegisterRequest{
    username: string;
    password: string;
    role: UserRole;
}