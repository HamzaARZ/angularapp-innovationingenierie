import { NatureProject } from './nature-project';
import { IClient } from './client';
import { ProjectStatus } from './project-status';
import { Observation } from './observation';


export interface IProject{
    id: string;
    natureProject: NatureProject;
    observation: Observation;
    projectStatus: ProjectStatus;
    architecte: string;
    dateEntree: Date;
    dateSortie: Date;
    prixPaye: Number;
    avance: Number;
    reste: Number;
    client: IClient;
}