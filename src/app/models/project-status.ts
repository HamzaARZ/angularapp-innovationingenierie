export enum ProjectStatus {
  DONE = "DONE",
  ON_GOING = "ON_GOING",
  PENDING = "PENDING"
}
