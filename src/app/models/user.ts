import { UserRole } from './UserRole';

export interface IUser{
    username: string;
    role: UserRole;
    enabled: Boolean
}