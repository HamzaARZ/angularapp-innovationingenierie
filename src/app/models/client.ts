export interface IClient{
    id: string;
    nom: string;
    prenom: string;
    numTelephone: string;
}