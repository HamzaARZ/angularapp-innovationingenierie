import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { RegisterRequest } from 'src/app/models/registerRequest';
import { AuthService } from 'src/app/services/auth.service';
import { UserRole } from 'src/app/models/UserRole';
import { UserService } from 'src/app/services/user.service';
import { UserComponent } from '../user/user.component';
import { IUser } from 'src/app/models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  roles: UserRole[] = [UserRole.USER, UserRole.MANAGER];

  registerForm: FormGroup;
  registerRequest: RegisterRequest;
  user: IUser;
  usernameUrl: string;

  isSending: Boolean = false;
  activationMessage = "an ADMIN should activate your account !";

  public errorMessage: String;
  public tokenExpiredMsg: String;
   

  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      password2: ['', Validators.required],
      role: ['', Validators.required],
    });

    this.usernameUrl = this.route.snapshot.params['username'];
    if (this.usernameUrl !== undefined) {
      this.userService.getUserByUsername(this.usernameUrl)
        .then(data => {
          let { username, role, enabled } = data;
          this.registerRequest = { username, role, password: null };

          this.registerForm.setValue({ username, role, password: null, password2: null });
          if(enabled){
            this.activationMessage = undefined;
          }
        })
        .catch(error => {
          if(error.error.message){
            alert(error.error.message);
          }
          this.router.navigateByUrl("users");
        });
    }
  }

  onSubmit() {
    this.tokenExpiredMsg = undefined;
    this.errorMessage = undefined;
    this.isSending = true;


    this.registerRequest = {
      username: this.username.value,
      password: this.password.value,
      role: this.role.value
    }

    if (this.usernameUrl !== undefined) {
      // updating
      if (window.confirm("are you sure you want to save changes ?")) {
        this.userService.updateUser(this.usernameUrl, this.registerRequest).subscribe(response => {
          console.log(response);
          this.router.navigateByUrl('/users');
        }, error => {
          console.log(error.status);
          alert(error.error.message);
          if(error.status === 403){
            this.tokenExpiredMsg = "Your token has expired";
          }

          this.isSending = false;
          this.password.setValue("");
          this.password2.setValue("");
          this.username.setValue(this.usernameUrl);
        });
      }else{
        this.isSending = false;
      }


    } else {
      this.authService.register(this.registerRequest).subscribe(response => {
        console.log(response);
        this.router.navigateByUrl('/login');
      }, err => {
        // this.errorMessage = "error registration";
        if (err && err.hasOwnProperty('error') && err.error.hasOwnProperty('errors') && err.error.errors.length > 0) {
          alert(err.error.errors.join('\n'));
        } else {
          alert(err.error.message);
        }
        this.isSending = false;
        this.password.setValue("");
        this.password2.setValue("");
      });
    }
  }

  reconnect() {
    this.authService.reconnect();
  }

  get username() {
    return this.registerForm.get('username');
  }

  get password() {
    return this.registerForm.get('password');
  }
  get password2() {
    return this.registerForm.get('password2');
  }

  get role() {
    return this.registerForm.get('role');
  }
}
