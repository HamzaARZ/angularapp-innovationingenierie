import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IProject } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';
import { ClientService } from 'src/app/services/client.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.scss']
})
export class ProjectEditComponent implements OnInit {

  public clients = [];
  public errorMessage;
  isSending: boolean;
  idUrl: string;
  changesConfirmed: Boolean;

  projectForm: FormGroup;

  public project: IProject;


  natures = ["R_PLUS_1", "R_PLUS_2", "R_PLUS_3", "R_PLUS_4"];
  observations = ["Etude", "EtudeEtSuivi"];
  allStatus = ["PENDING", "ON_GOING", "DONE"];

  constructor(private fb: FormBuilder,
    private _clientService: ClientService,
    private _projectService: ProjectService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.isSending = false;

    // get all clients from server
    this._clientService.getClients().subscribe(data => this.clients = data,
      error => this.errorMessage = error);

    this.idUrl = this.route.snapshot.params['id'];
    if (this.idUrl !== undefined) {
      //get projectToUpdate by id
      this._projectService.getProjectById(this.idUrl)
        .then(data => {
          this.project = data;

          let { natureProject, observation, projectStatus, architecte, prixPaye, avance, reste } = this.project;
          this.projectForm.setValue({ clientId: this.project.client.id, natureProject, observation, projectStatus, architecte, prixPaye, avance, reste });
        })
        .catch(error => alert(error));
    }



    this.projectForm = this.fb.group({

      clientId: ['', Validators.required],
      natureProject: ['', Validators.required],
      observation: ['', Validators.required],
      projectStatus: ['', Validators.required],

      architecte: ['', Validators.required],

      prixPaye: ['', [
        Validators.required,
        Validators.pattern('[0-9]*')
      ]],
      avance: ['', [
        Validators.required,
        Validators.pattern('[0-9]*')
      ]],
      reste: ['', [
        Validators.required,
        Validators.pattern('[0-9]*')
      ]]
    });

  }

  onSubmit() {
    if (this.projectForm.valid) {
      this.isSending = true;
      this.changesConfirmed = true;


      this.project = {
        id: this.project === undefined ? '' : this.project.id,
        natureProject: this.natureProject.value,
        observation: this.observation.value,
        projectStatus: this.projectStatus.value,
        architecte: this.architecte.value,
        dateEntree: this.project === undefined ? null : new Date(this.project.dateEntree),// !!!
        dateSortie: null,
        prixPaye: this.prixPaye.value,
        avance: this.avance.value,
        reste: this.reste.value,
        client: {
          id: this.clientId.value,
          nom: null,
          prenom: null,
          numTelephone: null
        }
      };

      if (this.idUrl !== undefined && !window.confirm("are you sure you want to save changes ?")) {
        this.changesConfirmed = false;
      }
      console.log(this.project);

      if (this.changesConfirmed) {
        this._projectService.createOrUpdateProject(this.project).subscribe(response => {
          this.router.navigateByUrl('/projects');
        }, err => {
          console.log('error sending !!');
          console.log(err);
          if (err && err.hasOwnProperty('error') && err.error.errors.length > 0) {
            alert(err.error.errors.join('\n'));
            this.isSending = false;
          }
        });
      } else {
        this.isSending = false;
      }

      // this.reset();
    } else {
      alert('The value you intered is invalid!');
    }
  }

  reconnect() {
    this.authService.reconnect();
  }

  reset() {
    this.projectForm.reset();
  }


  get clientId() {
    return this.projectForm.get('clientId');
  }


  get natureProject() {
    return this.projectForm.get('natureProject');
  }
  get observation() {
    return this.projectForm.get('observation');
  }
  get projectStatus() {
    return this.projectForm.get('projectStatus');
  }
  get architecte() {
    return this.projectForm.get('architecte');
  }
  get prixPaye() {
    return this.projectForm.get('prixPaye');
  }
  get avance() {
    return this.projectForm.get('avance');
  }
  get reste() {
    return this.projectForm.get('reste');
  }

}
