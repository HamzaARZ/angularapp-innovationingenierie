import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IUser } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  userFilterForm: FormGroup;

  public users: IUser[] = [];

  // public userFilter = {
  //   usernameFilter: '',
  //   roleFilter: '',
  //   enabledFilter: ''
  // }

  public errorMessage;
  public tokenExpiredMsg;

  constructor(private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.getUsers();

    this.userFilterForm = this.fb.group({
      usernameFilter: [''],
      adminRoleFilter: [true],
      managerRoleFilter: [true],
      userRoleFilter: [true],
      enabledFilter: ['']
    });

    // this.userFilterForm.valueChanges.subscribe(values => {
    //   console.log(values);
    // });
  }

  getUsers() {
    this.userService.getUsers().subscribe(data => {
      this.users = data;
    },// !!!
      error => {
        
        if (error.status === 403) {
          this.tokenExpiredMsg = "Your token has expired";
        } else {
          this.errorMessage = "Can Not Connect To The Server";
        }
      });
  }

  reconnect() {
    this.authService.reconnect();
  }


  activateUser(username: string) {
    if (window.confirm('Are sure you want to activate this user Account ?')) {
      this.userService.activateUser(username).subscribe(
        response => {
          alert(response.message);
          this.getUsers();
        },
        error => alert(error.error.message));
        this.getUsers();
    }
  }

  disableUser(username: string) {
    if (window.confirm('Are sure you want to disable this user Account ?')) {
      this.userService.disableUser(username).subscribe(
        response => {
          alert(response.message);
          this.getUsers();
        },
        error => alert(error.error.message));

        this.getUsers();
    }
  }

  editUser(username: String) {
    this.router.navigate(['register', {username: username}]);
  }

  deleteUser(username: string) {
    if (window.confirm('Are sure you want to delete this user ?')) {
      this.userService.deleteUser(username).subscribe(
        response => {
          this.getUsers();
        },
        error => alert(error.error.message));
    }
  }

  filterUser(user: IUser): Boolean{
    return user.username.includes(this.usernameFilter.value) &&
    String(user.enabled).includes(this.enabledFilter.value) &&
    ((user.role.toString() === 'ADMIN' && this.adminRoleFilter.value) ||
      (user.role.toString() === 'MANAGER' && this.managerRoleFilter.value) ||
      (user.role.toString() === 'USER' && this.userRoleFilter.value));
  }

  hasAuthority(authority: string): Boolean{
    return this.authService.hasAuthority(authority);
  }

  get usernameFilter(){
    return this.userFilterForm.get('usernameFilter');
  }
  get enabledFilter(){
    return this.userFilterForm.get('enabledFilter');
  }
  get adminRoleFilter(){
    return this.userFilterForm.get('adminRoleFilter');
  }
  get managerRoleFilter(){
    return this.userFilterForm.get('managerRoleFilter');
  }
  get userRoleFilter(){
    return this.userFilterForm.get('userRoleFilter');
  }


}
