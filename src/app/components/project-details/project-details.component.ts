import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { IProject } from '../../models/project';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ProjectComponent } from 'src/app/components/project/project.component';
import { HttpResponse } from '@angular/common/http';
import { map, tap, last, catchError } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {

  fileForm: FormGroup;


  @Output("getProjectsFunction") getProjectsFunction: EventEmitter<any> = new EventEmitter();

  @Input() project: IProject;
  @ViewChild('filesInput', { static: false }) filesInput: ElementRef;

  showInputFiles: Boolean = false;

  displayFiles = false;
  projectFiles = [];

  public files = [];
  public errorMessage;

  constructor(private _projectService: ProjectService,
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router) { }


  ngOnInit() {
    this.getProjectFiles();
    this.fileForm = this.fb.group({
      filesInputField: ['']
    });
  }

  // onSubmit(event) {
  //   event.preventDefault();
  //   console.log('Submit clicked');
  //   this._projectService.pushFileToStorage(this.files, this.project.id)
  //     .subscribe(
  //       resp => {
  //         this.onResetForm();
  //         this.getProjectFiles();
  //         alert(resp);
  //       },
  //       err => console.log(err)
  //     )
  // }
  onSubmit() {
    console.log('Submit clicked');
    console.log(this.filesInputField.value);
    this._projectService.pushFileToStorage(this.filesInputField.value.files, this.project.id)
      .subscribe(
        resp => {
          this.onResetForm();
          this.getProjectFiles();
          alert(resp.message);
        },
        err => {
          console.log("err :");
          console.log(err);
        }
      );
  }

  hasAuthority(authority: string): Boolean{
    return this.authService.hasAuthority(authority);
  }


  getProjectFiles() {
    this._projectService.getProjectFiles(this.project.id).subscribe(
      data => {
        this.projectFiles = data;
      },
      error => alert(error));
  }

  showHideFiles() {
    if (!this.displayFiles) {
      this.displayFiles = true;
      this.getProjectFiles();
    } else {
      this.displayFiles = false;
    }
  }



  onFileUploadChange(event) {
    this.files = event.target.files;
  }

  deleteProject() {
    if (window.confirm('are you sure you want to delete this Project ?')) {
      this._projectService.deleteProject(this.project.id).subscribe(
        response => {
          this.getProjectsFunction.emit();
        },
        error => console.log(error));
    }
  }

  deleteFile(file) {
    if (window.confirm('are you sure you want to delete this File ?')) {
      this._projectService.deleteFile(this.project.id, file).subscribe(
        response => {
          this.getProjectFiles();
        },
        error => console.log(error));
    }
  }

  downloadFile(file: string) {
    this._projectService.downloadFile(this.project.id, file);
  }

  // downloadFile(file: string){
  //   this._projectService.downloadFile(this.project.id, file).subscribe(
  //     (response: any) =>{
  //         let dataType = response.type;
  //         let binaryData = [];
  //         binaryData.push(response);
  //         let downloadLink = document.createElement('a');
  //         downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
  //         if (file)
  //             downloadLink.setAttribute('download', file);
  //         document.body.appendChild(downloadLink);
  //         downloadLink.click();
  //     }
  // );
  // }

  editProject() {
    this.router.navigate(['projects/edit', { id: this.project.id }]);

    // set project in localStorage

    // localStorage.setItem('projectToUpdate', JSON.stringify(this.project));
    // this.router.navigate(['projects/edit']);
  }

  onResetForm() {
    this.filesInputField.setValue('');
    // this.filesInput.nativeElement.value = "";
  }
  get filesInputField(){
    return this.fileForm.get('filesInputField');
  }


}