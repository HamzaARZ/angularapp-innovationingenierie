import { Component, OnInit } from '@angular/core';
import { ClientService } from 'src/app/services/client.service';//**************
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IClient } from 'src/app/models/client';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {

  clientFilterForm: FormGroup;

  public clients: IClient[] = [];
  public clientsFiltred: IClient[] = [];
  public errorMessage;
  public tokenExpiredMsg;

  public clientFilter = {
    nomFilter: '',
    prenomFilter: '',
    numTelephoneFilter: ''
  };

  constructor(private _clientService: ClientService,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder) {}

  ngOnInit() {
    this.getClients();

    this.clientFilterForm = this.fb.group({
      nomFilter: [''],
      prenomFilter: [''],
      numTelephoneFilter: ['', Validators.pattern('[0-9]*')]
    });

    this.clientFilterForm.valueChanges.subscribe(values => {
      this.clientFilter = values;
      this.updateClientsFiltred();
    });
  }


  reconnect(){
    this.authService.reconnect();
  }

  getClients(){
    this._clientService.getClients().subscribe(data => {
      this.clientsFiltred = this.clients = data;
    },// !!!
      error => {
        if(error.status === 403){
          this.tokenExpiredMsg = "your token has expired"
        }else{
          this.errorMessage = "Can Not Connect To The Server";
        }
      });
  }
  // Can Not Connect To The Server

  updateClientsFiltred(){
    this.clientsFiltred = [];
    this.clients.forEach(client => {
      if(client.nom.includes(this.clientFilter.nomFilter) &&
      client.prenom.includes(this.clientFilter.prenomFilter) &&
      client.numTelephone.indexOf(this.clientFilter.numTelephoneFilter) === 0){
        this.clientsFiltred.push(client);
      }
    })
  }


  deleteClient(clientId) {
    if(window.confirm('Are sure you want to delete this client ?')){
    this._clientService.deleteClient(clientId).subscribe(
      response => {
        this.getClients();
      },
      error => alert(error.error.message));
    }
  }

  hasAuthority(authority: string): Boolean{
    return this.authService.hasAuthority(authority);
  }


  editClient(clientId) {
    this.router.navigate(['clients/edit', {id: clientId}]);
  }

  get nomFilter(){
    return this.clientFilterForm.get('nomFilter');
  }
  get prenomFilter(){
    return this.clientFilterForm.get('prenomFilter');
  }
  get numTelephoneFilter(){
    return this.clientFilterForm.get('numTelephoneFilter');
  }




}
