import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IProject } from 'src/app/models/project';
import { AuthService } from 'src/app/services/auth.service';
import { ClientService } from 'src/app/services/client.service';
import { IClient } from 'src/app/models/client';
// import { MatButtonModule } from '@angular/material';
// import { MatFileUploadModule } from 'angular-material-fileupload';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
})
export class ProjectComponent implements OnInit {

  @ViewChild('filtersDiv', { static: false }) filtersDiv: ElementRef;

  fileForm: FormGroup;

  projectFilterForm: FormGroup;

  public clients: IClient[] = [];
  public projects: IProject[] = [];
  public projectsFiltred: IProject[] = [];

  public natures = ["R_PLUS_1", "R_PLUS_2", "R_PLUS_3", "R_PLUS_4"];
  public observations = ["Etude", "EtudeEtSuivi"];
  public allStatus = ["PENDING", "ON_GOING", "DONE"];

  public showFilters: Boolean;

  public projectFilter = {
    statusFilter: '',
    natureFilter: '',
    observationFilter: []
  }

  public errorMessage;
  public tokenExpiredMsg;


  constructor(private _projectService: ProjectService,
    private clientService: ClientService,
    private authService: AuthService,
    private fb: FormBuilder) { }



  ngOnInit() {
    this.getProjects();
    this.getClients();

    this.projectFilterForm = this.fb.group({
      clientIdFilter: [null],
      architecteFilter: [null],

      natureFilter: [null],
      observationFilter: [null],
      statusFilter: [null],

      date: [null]
    });

    
    this.projectFilterForm.valueChanges.subscribe(values => {
      this.updateProjectsFiltred();
    });
    
  }



  // onValuesChange(champ, value, checked) {

  //   switch (champ) {
  //     case "observation": {
  //       let index = this.projectFilter.observationFilter.indexOf(value);
  //       if (checked && index === -1) {
  //         this.projectFilter.observationFilter.push(value);
  //       }
  //       else if (!checked && index > -1) {
  //         this.projectFilter.observationFilter.splice(index, 1);
  //       }
  //       break;
  //     }
  //   }
  // }


  updateProjectsFiltred() {
    this.projectsFiltred = [];

    this.projects.forEach(project => {

      if ((!this.clientIdFilter.value || project.client.id == (this.clientIdFilter.value)) &&
        (!this.architecteFilter.value || project.architecte.includes(this.architecteFilter.value)) &&
        (!this.statusFilter.value || project.projectStatus.includes(this.statusFilter.value)) &&
        (!this.natureFilter.value || project.natureProject.includes(this.natureFilter.value)) &&
        (!this.observationFilter.value || project.observation == (this.observationFilter.value)) 
        // project.dateEntree > this.date.value
        )
        {
        this.projectsFiltred.push(project);
      }
    });

  }


  showHideFilters(){
    this.projectFilterForm.reset();
    this.showFilters = !this.showFilters;
  }

  hasAuthority(authority: string): Boolean{
    return this.authService.hasAuthority(authority);
  }


  getProjects() {
    this._projectService.getProjects().subscribe(data => {
      this.projectsFiltred = this.projects = data
    },
      error => {
        if (error.status === 403) {
          this.tokenExpiredMsg = "your token has expired";
        } else {
          this.errorMessage = "Can Not Connect To The Server";
        }
      });
  }

  getClients() {
    this.clientService.getClients().subscribe(data => this.clients = data,
      error => this.errorMessage = error);
  }

  reconnect() {
    this.authService.reconnect();
  }

  get statusFilter(){
    return this.projectFilterForm.get(('statusFilter'));
  }
  get natureFilter(){
    return this.projectFilterForm.get(('natureFilter'));
  }
  get observationFilter(){
    return this.projectFilterForm.get(('observationFilter'));
  }

  get clientIdFilter(){
    return this.projectFilterForm.get(('clientIdFilter'));
  }
  get architecteFilter(){
    return this.projectFilterForm.get(('architecteFilter'));
  }
  get date(){
    return this.projectFilterForm.get('date');
  }

}