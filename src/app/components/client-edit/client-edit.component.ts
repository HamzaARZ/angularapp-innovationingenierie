import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IClient } from 'src/app/models/client';
import { ClientService } from 'src/app/services/client.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.scss']
})
export class ClientEditComponent implements OnInit {

  clientForm: FormGroup;

  client: IClient;

  isSending: boolean;
  idUrl: string;
  changesConfirmed: Boolean;

  public errorMessage;
  public tokenExpiredMsg;

  constructor(private fb: FormBuilder,
    private clientApi: ClientService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute) { }



  ngOnInit() {

    this.isSending = false;
    this.clientForm = this.fb.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      numTelephone: ['', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10),
        Validators.pattern('[0-9]*')
      ]]
    });

    this.idUrl = this.route.snapshot.params['id'];
    if (this.idUrl !== undefined) {
      this.clientApi.getClientById(this.idUrl)
        .then(data => {
          this.client = data;
          console.log(data);
          let { nom, prenom, numTelephone } = this.client;
          this.clientForm.setValue({ nom, prenom, numTelephone });
        })
        .catch(error => {
        if (error.error.message) {
          alert(error.error.message);
        }
        this.router.navigateByUrl("clients");
          
        });
    }
  }

  onSubmit() {
    if (this.clientForm.valid) {
      this.tokenExpiredMsg = undefined;
      this.isSending = true;
      this.changesConfirmed = true;

      this.client = {
        id: this.idUrl === undefined ? '' : this.idUrl,
        nom: this.nom.value,
        prenom: this.prenom.value,
        numTelephone: this.numTelephone.value
      };
      if (this.idUrl !== undefined && !window.confirm("are you sure you want to save changes ?")) {
        this.changesConfirmed = false;
      }
      if(this.changesConfirmed){
        this.clientApi.createOrUpdateClient(this.client).subscribe(response => {
          this.router.navigateByUrl('/clients');
        }, err => {
          if (err.status === 403) {
            this.tokenExpiredMsg = "your token has expired";
          }else if(err && err.hasOwnProperty('error') && err.error.hasOwnProperty('errors') && err.error.errors.length > 0) {
            alert(err.error.errors.join('\n'));
          }else{
            this.errorMessage = "Can Not Connect To The Server";
          }
        });
      }
      this.isSending = false;
      
        
    } else {
      alert('The value you intered are invalid!');
    }
    // if valid send to server
    //  if server response is OK then go back to clients
    //  else show the error message given by the server
    // if invalid show errors in the form
  }

  reconnect() {
    this.authService.reconnect();
  }
  reset() {
    this.clientForm.reset();
  }

  get nom() {
    return this.clientForm.get('nom');
  }

  get prenom() {
    return this.clientForm.get('prenom');
  }

  get numTelephone() {
    return this.clientForm.get('numTelephone');
  }

}
