import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginRequest } from 'src/app/models/loginRequest';
import { AuthService } from 'src/app/services/auth.service'
import { error } from 'protractor';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginRequest: LoginRequest;

  isSending: Boolean = false;
  loginError: String;
  hidePassword: Boolean = true;

  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });

    
    this.password.valueChanges.subscribe(value => {
      if(value !== ""){
        this.loginError = undefined;
      }
        
    })
  }

  

  onSubmit() {
    this.loginError = undefined;
    this.isSending = true;
    this.loginRequest = {
      username: this.username.value,
      password: this.password.value
    }

    this.authService.login(this.loginRequest).subscribe(response => {

      this.authService.setUsername(response.username);
      this.authService.setAuthorities(response.authorities);
      this.authService.setToken(response.token);
      
      this.router.navigateByUrl('/home');
    }, error => {
      if(error.status !== 403){
        this.loginError = "Can Not Connect To The Server";
      }else{
        this.loginError = error.error.message;
      }
      


      this.isSending = false;
      this.password.setValue("");
    });
  }
  
  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }
}
