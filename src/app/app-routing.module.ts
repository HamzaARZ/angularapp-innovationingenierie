import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientComponent } from './components/client/client.component';
import { HomeComponent } from './components/home/home.component';
import { ProjectComponent } from './components/project/project.component';
import { ClientEditComponent } from './components/client-edit/client-edit.component';
import { ProjectEditComponent } from './components/project-edit/project-edit.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserComponent } from './components/user/user.component';




const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'home', component: HomeComponent},
  { path: 'clients', component: ClientComponent},
  { path: 'clients/edit', component: ClientEditComponent},
  { path: 'projects', component: ProjectComponent},
  { path: 'projects/edit', component: ProjectEditComponent}, // /:id
  { path: 'users', component: UserComponent},

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LoginComponent, HomeComponent, ClientComponent, 
  ClientEditComponent, ProjectComponent, ProjectEditComponent,
  RegisterComponent
]
